class Factory {
 

    void factoryName(){
        System.out.println("Made by Honda");
    }

    public static void main(String[] args){
        Matic m = new Matic();
        m.factoryName();
        m.print();
        System.out.println("Nama Kendaraan : " + m.name);
        System.out.println("Harga  : " + m.price +  m.valuta);

        Sedan s = new Sedan();
        s.factoryName();
        System.out.println("Nama Kendaraan : " + s.name);
        System.out.println("Harga  : " + s.price +  s.valuta);

        Sport p = new Sport();
        p.factoryName();
        System.out.println("Nama Kendaraan : " + p.name);
        System.out.println("Harga  : " + p.price +  p.valuta);

        Suv u = new Suv();
        u.factoryName();
        System.out.println("Nama Kendaraan : " + u.name);
        System.out.println("Harga  : " + u.price +  u.valuta);

        Bebek b = new Bebek();
        b.factoryName();
        System.out.println("Nama Kendaraan : " + b.name);
        System.out.println("Harga  : " + b.price +  b.valuta);

        CityCar c = new CityCar();
        c.factoryName();
        System.out.println("Nama Kendaraan : " + c.name);
        System.out.println("Harga  : " + c.price +  c.valuta);



        
    }
}